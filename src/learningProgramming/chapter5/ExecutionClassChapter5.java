package learningProgramming.chapter5;

import static learningProgramming.chapter5.DayOfTheWeekChallenge.printDayOfTheWeek;
import static learningProgramming.chapter5.DigitSumChallenge.sumDigits;
import static learningProgramming.chapter5.EvenDigitSum.getEvenDigitSum;
import static learningProgramming.chapter5.FirstLastDigitSum.sumFirstAndLastDigit;
import static learningProgramming.chapter5.ForLoop.calculateInterest;
import static learningProgramming.chapter5.ForLoop.isPrime;
import static learningProgramming.chapter5.GreatestCommonDivisor.getGreatestCommonDivisor;
import static learningProgramming.chapter5.LastDigitChecker.hasSameLastDigit;
import static learningProgramming.chapter5.NumberInWord.printNumberInWord;
import static learningProgramming.chapter5.NumberOfDaysInMonth.getDaysInMonth;
import static learningProgramming.chapter5.NumberPalindrome.isPalindrome;
import static learningProgramming.chapter5.SharedDigit.hasSharedDigit;
import static learningProgramming.chapter5.SumOddRange.sumOdd;
import static learningProgramming.chapter5.WhileAndDoWhile.isEvenNumber;

public class ExecutionClassChapter5 {

    public static void main(String[] args) {

        for (int i = -1; i < 7; i++) {
            printDayOfTheWeek(i);
        }
        System.out.println("******************************");

        for (int i = -1; i < 11; i++) {
            printNumberInWord(i);
        }
        System.out.println("******************************");

        System.out.println(getDaysInMonth(1, 2020)); /*should return 31 since January has 31 days.*/
        System.out.println(getDaysInMonth(
            2,
            2020)); /*should return 29 since February has 29 days in a leap year and 2020 is a leap year.*/
        System.out.println(getDaysInMonth(
            2,
            2018)); /*should return 28 since February has 28 days if it's not a leap year and 2018 is not a leap year.*/
        System.out.println(getDaysInMonth(-1, 2020)); /*should return -1 since the parameter month is invalid.*/
        System.out.println(getDaysInMonth(
            1,
            -2020)); /*should return -1 since the parameter year is outside the range of 1 to 9999.*/

        System.out.println("******************************");

        for (int i = 2; i < 9; i++) {
            System.out.println("10,000 at " + i + "% interest = " + String.format("%.2f", calculateInterest(10000.0, i)));
        }

        System.out.println("******************************");
        for (int i = 8; i > 1; i--) {
            System.out.println("10,000 at " + i + "% interest = " + String.format("%.2f", calculateInterest(10000.0, i)));
        }

        System.out.println("******************************");
        int numberOfPrimeNumbers = 0;
        for (int i = 10; i < 1000; i++) {
            if (isPrime(i)) {
                if (numberOfPrimeNumbers <= 10) {
                    numberOfPrimeNumbers++;
                    System.out.println("Number " + i + " is a prime number");
                } else { break; }
            }
        }

        System.out.println("******************************");
        /*Sum 3 and 5 Challenge*/
        int sum = 0;
        int count = 0;
        for (int i = 1; i <= 1000; i++) {
            if ((i % 3 == 0) && (i % 5 == 0)) {
                System.out.println("Number " + i + " is divided by 3 and 5");
                count++;
                sum += i;
            }
            if (count == 5) {
                break;
            }
        }
        System.out.println("Sum of all the numbers is: " + sum);

        System.out.println("******************************");
        System.out.println(sumOdd(1, 100)); /*2500*/
        System.out.println(sumOdd(-1, 100));/*-1*/
        System.out.println(sumOdd(100, 100));/*0*/
        System.out.println(sumOdd(13, 13));/*13*/
        System.out.println(sumOdd(100, -100));/*-1*/
        System.out.println(sumOdd(100, 1000));/*247500*/

        System.out.println("******************************");
        /*
                Make a record with total number of even numbers it has found
                and break once are 5 found
                and at the end, display the total number of even numbers found
        */
        int startNumber = 4;
        int finishNumber = 20;
        int evenNumbersFound = 0;

        while (startNumber <= finishNumber) {
            startNumber++;
            if (!isEvenNumber(startNumber)) {
                continue;
            }
            System.out.println("Even number " + startNumber);

            evenNumbersFound++;
            if (evenNumbersFound >= 5) {
                break;
            }
        }
        System.out.println("Total even numbers founded = " + evenNumbersFound);

        System.out.println("******************************");
        System.out.println("The sum of the digits in number 125 is " + sumDigits(125));
        System.out.println("The sum of the digits in number -125 is " + sumDigits(-125));
        System.out.println("The sum of the digits in number 4 is " + sumDigits(4));
        System.out.println("The sum of the digits in number 32123 is " + sumDigits(32123));

        System.out.println("******************************");
        System.out.println(isPalindrome(-1221));
        System.out.println(isPalindrome(707));
        System.out.println(isPalindrome(11212));

        System.out.println("******************************");
        System.out.println("The sum of first and last digits of number 252 is " + sumFirstAndLastDigit(252));
        System.out.println("The sum of first and last digits of number 257 is " + sumFirstAndLastDigit(257));
        System.out.println("The sum of first and last digits of number 0 is " + sumFirstAndLastDigit(0));
        System.out.println("The sum of first and last digits of number 5 is " + sumFirstAndLastDigit(5));
        System.out.println("The sum of first and last digits of number -10 is " + sumFirstAndLastDigit(-10));

        System.out.println("******************************");
//      getEvenDigitSum(123456789); → should return 20 since 2 + 4 + 6 + 8 = 20
        System.out.println("The sum of even digits of number 123456789 is " + getEvenDigitSum(123456789));
//      getEvenDigitSum(252); → should return 4 since 2 + 2 = 4
        System.out.println("The sum of even digits of number 252 is " + getEvenDigitSum(252));
//      getEvenDigitSum(-22); → should return -1 since the number is negative;
        System.out.println("The sum of even digits of number -22 is " + getEvenDigitSum(-22));

        System.out.println("******************************");
        System.out.println("The numbers 12 and 23 has shared digits " + hasSharedDigit(12, 23));
        System.out.println("The numbers 9 and 99 has shared digits " + hasSharedDigit(9, 99));
        System.out.println("The numbers 15 and 55 has shared digits " + hasSharedDigit(15, 55));

        System.out.println("******************************");
        System.out.println("The numbers 41, 22 and 71 has same last digits " + hasSameLastDigit(41, 22, 71));
        System.out.println("The numbers 23, 32 and 42 has same last digits " + hasSameLastDigit(23, 32, 42));
        System.out.println("The numbers 9, 99 and 999 has same last digits " + hasSameLastDigit(9, 99, 999));

        System.out.println("******************************");
        System.out.println("The greatest common divisor of 25 and 15 is " + getGreatestCommonDivisor(25, 15));
        System.out.println("The greatest common divisor of 12 and 30 is " + getGreatestCommonDivisor(12, 30));
        System.out.println("The greatest common divisor of 9 and 18 is " + getGreatestCommonDivisor(9, 18));
        System.out.println("The greatest common divisor of 81 and 153 is " + getGreatestCommonDivisor(81, 153));
    }
}
