/*
        Create a method called isEvenNumber that takes a parameter of type int
        It's purpose is to determine if the argument passed to the method is
        an even number or not.
        Return true if an even number, otherwise return false;
*/

package learningProgramming.chapter5;

public class WhileAndDoWhile {

    public static Boolean isEvenNumber(int number) {
        if ((number % 2) == 0) {
            return true;
        } else {
            return false;
        }
    }

}
