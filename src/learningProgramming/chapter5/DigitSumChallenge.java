/*
        Add some code to the main method to test out the sumDigits method to determine
        that it is working correctly for valid and invalid values passed as arguments.
*/

package learningProgramming.chapter5;

public class DigitSumChallenge {

    public static int sumDigits(int number) {
        if (number <= 10) {
            return -1;
        }

        int sum = 0;

        // 125 -> 125 /10 = 12 -> 12 * 10 = 120 -> 125 - 120 = 5

        while (number > 0) {
            // extract the least-significant digit
            int digit = number % 10;
            sum += digit;

            // drop the least-significant digit
            number/= 10;
        }

        return sum;
    }
}
