package learningProgramming.chapter5;

public class ForLoop {

    public static boolean isPrime(int n) {
        if (n == 1) {
            return false;
        }
        for (int i=2; i<= (long) Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static double calculateInterest(double amount, double interestRtae) {
        return (amount * (interestRtae/100));
    }

}
