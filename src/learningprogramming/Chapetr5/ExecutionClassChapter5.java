package learningprogramming.Chapetr5;

public class ExecutionClassChapter5 {
    public static void main(String[] args) {

        for (int i = -1; i < 7; i++){
            DayOfTheWeekChallenge.printDayOfTheWeek(i);
        }
        System.out.println("******************************");

        for (int i = -1; i < 11; i++){
            NumberInWord.printNumberInWord(i);
        }
        System.out.println("******************************");

        System.out.println(NumberOfDaysInMonth.getDaysInMonth(1, 2020)); /*should return 31 since January has 31 days.*/
        System.out.println(NumberOfDaysInMonth.getDaysInMonth(2, 2020)); /*should return 29 since February has 29 days in a leap year and 2020 is a leap year.*/
        System.out.println(NumberOfDaysInMonth.getDaysInMonth(2, 2018)); /*should return 28 since February has 28 days if it's not a leap year and 2018 is not a leap year.*/
        System.out.println(NumberOfDaysInMonth.getDaysInMonth(-1, 2020)); /*should return -1 since the parameter month is invalid.*/
        System.out.println(NumberOfDaysInMonth.getDaysInMonth(1, -2020)); /*should return -1 since the parameter year is outside the range of 1 to 9999.*/

        System.out.println("******************************");

        for (int i=2; i<9; i++){
            System.out.println("10,000 at " + i + "% interest = " + String.format("%.2f",ForLoop.calculateInterest(10000.0,i)));
        }

        System.out.println("******************************");
        for (int i=8; i>1; i--){
            System.out.println("10,000 at " + i + "% interest = " + String.format("%.2f",ForLoop.calculateInterest(10000.0,i)));
        }

        System.out.println("******************************");
        int numberOfPrimeNumbers = 0;
        for (int i=10; i < 1000; i++) {
            if (ForLoop.isPrime(i)) {
                if (numberOfPrimeNumbers <= 10) {
                    numberOfPrimeNumbers++;
                    System.out.println("Number " + i + " is a prime number");
                } else break;
            }
        }

        System.out.println("******************************");
        /*Sum 3 and 5 Challenge*/
        int sum = 0;
        int count = 0;
        for (int i = 1; i <= 1000; i++) {
            if ((i % 3 == 0) && (i % 5 == 0)) {
                System.out.println("Number " + i + " is divided by 3 and 5");
                count++;
                sum += i;
            }
                if (count == 5) {
                    break;
                }
        }
        System.out.println("Sum of all the numbers is: " + sum);

        System.out.println("******************************");
        System.out.println(SumOddRange.sumOdd(1,100)); /*2500*/
        System.out.println(SumOddRange.sumOdd(-1,100));/*-1*/
        System.out.println(SumOddRange.sumOdd(100,100));/*0*/
        System.out.println(SumOddRange.sumOdd(13,13));/*13*/
        System.out.println(SumOddRange.sumOdd(100,-100));/*-1*/
        System.out.println(SumOddRange.sumOdd(100,1000));/*247500*/

        System.out.println("******************************");
        System.out.println("******************************");
        System.out.println("******************************");
        System.out.println("******************************");
    }
}
